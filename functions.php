<?php

/**
 * @package Boldface\PakMail
 */
declare( strict_types = 1 );
namespace Boldface\PakMail;

/**
 * Use the Bootstrap auto updater
 *
 * @since 0.1
 */
function updater_init() {
  require __DIR__ . '/src/updater.php';
  $updater = new updater();
  $updater->init();
}
\add_action( 'init', __NAMESPACE__ . '\updater_init' );

/**
 * Filter the modules array.
 *
 * @since 0.1
 *
 * @param array $modules The array of modules.
 *
 * @return array The modified array of modules.
 */
function modulesList( array $modules ) : array {
  $modules[] = 'googleFonts';
  $modules[] = 'widgets';
  return $modules;
}
\add_filter( 'Boldface\Bootstrap\Controllers\modules', __NAMESPACE__ . '\modulesList' );

/**
 * Filter the googleFonts array.
 *
 * @since 0.1
 *
 * @param array $fonts The array of googleFonts.
 *
 * @return array The modified array of googleFonts.
 */
function googleFonts( array $fonts ) : array {
  $fonts[] = 'Oswald';
  $fonts[] = 'PT Sans';
  $fonts[] = 'PT Sans Narrow';
  return $fonts;
}
\add_filter( 'Boldface\Bootstrap\Models\googleFonts', __NAMESPACE__ . '\googleFonts' );

/**
 * Filter the sidebars array.
 *
 * @since 0.1
 *
 * @param array $sidebars The array of sidebars.
 *
 * @return array The modified array of sidebars.
 */
function sidebars( array $sidebars ) : array {
  $sidebars[] = [
    'name'          => 'Header',
    'id'            => 'header',
    'description'   => 'Header widgets',
    'before_widget' => '<div class="widget header-widget %2$s">',
    'after_widget'  => '</div>',
  ];
  $sidebars[] = [
    'name'          => 'Footer',
    'id'            => 'footer',
    'description'   => 'Footer widgets',
    'before_widget' => '<div class="widget footer-widget %2$s">',
    'after_widget'  => '</div>',
  ];  return $sidebars;
}
\add_filter( 'Boldface\Bootstrap\Models\widgets\sidebars', __NAMESPACE__ . '\sidebars' );

/**
 * Filter the navigation class.
 *
 * @since 0.1
 *
 * @param string $class The navigation class.
 *
 * @return array The modified navigation class.
 */
function navigationClass( string $class ) : string {
  return str_replace( [ 'navbar-dark', 'bg-dark' ], [ 'navbar-light', 'bg-white' ], $class );
}
\add_filter( 'Boldface\Bootstrap\Views\navigation\class', __NAMESPACE__ . '\navigationClass' );

/**
 * Enqueue block editor assets.
 *
 * @since 0.1
 */
function enqueue_block_editor_assets() {
  \wp_enqueue_script(
    'pakmail-editor-assets',
    \get_stylesheet_directory_uri() . '/assets/js/pakmail.js',
    [ 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', ]
  );
  \wp_enqueue_style(
    'pakmail-cta',
    \get_stylesheet_directory_uri() . '/assets/css/cta.css',
    []
  );
}
\add_action( 'enqueue_block_editor_assets', __NAMESPACE__ . '\enqueue_block_editor_assets' );

/**
 * Enqueue sripts and styles.
 *
 * @since 0.1
 */
function wp_enqueue_scripts() {
  \wp_enqueue_script(
    'pakmail-nav',
    \get_stylesheet_directory_uri() . '/assets/js/nav.js',
    [ 'jquery', ]
  );
  if( \is_front_page() ) {
    \wp_enqueue_script(
      'pakmail-cta',
      \get_stylesheet_directory_uri() . '/assets/js/cta.js',
      [ 'jquery', ]
    );
  }
}
\add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\wp_enqueue_scripts' );

/**
 * Filter the navigation menu by appending the header widgets.
 *
 * @since 0.1
 *
 * @param string $menu The navigation menu.
 *
 * @return string The modified navigation menu.
 */
function navigationMenu( string $menu ) : string {
  if( ! \is_active_sidebar( 'header' ) ) {
    return $menu;
  }
  ob_start();
  \dynamic_sidebar( 'header' );
  return $menu . ob_get_clean();
}
\add_filter( 'Boldface\Bootstrap\Views\navigation\menu', __NAMESPACE__ . '\navigationMenu', 11 );

/**
 * Filter the footer text to add copyright information.
 *
 * @since 0.1
 *
 * @param string $modules The footer text.
 *
 * @return string The modified footer text.
 */
function footerText( string $text ) : string {
  return sprintf( '&copy; %1$s PakMail All Rights Reserved.', date( 'Y' ) );
}
\add_filter( 'Boldface\Bootstrap\Views\footer\text', __NAMESPACE__ . '\footerText' );

/**
 * Filter the footer class.
 *
 * @since 0.1
 *
 * @param string $modules The footer class.
 *
 * @return string The modified footer class.
 */
function footerClass( string $class ) : string {
  $class = str_replace( [ ' fixed-bottom', ' bg-light' ], '', $class );
  return $class .= ' bg-white';
}
\add_filter( 'Boldface\Bootstrap\Views\footer\class', __NAMESPACE__ . '\footerClass' );

/**
 * Filter the footer wrapper class.
 *
 * @since 0.1
 *
 * @param string $modules The footer wrapper class.
 *
 * @return string The modified foote wrapper class.
 */
function footerWrapperClass( string $class ) : string {
  return $class . ' justify-content-center';
}
\add_filter( 'Boldface\Bootstrap\Views\footer\wrapper\class', __NAMESPACE__ . '\footerWrapperClass' );

/**
 * Filter the footer by prepending the footer widgets.
 *
 * @since 0.1
 *
 * @param array $footer The footer.
 *
 * @return array The modified footer.
 */
function footer( string $footer ) : string {
  if( ! \is_active_sidebar( 'footer' ) ) {
    return $footer;
  }
  ob_start();
  \dynamic_sidebar( 'footer' );
  $footerWidgets = ob_get_clean();

  $widgets = sprintf(
    '<aside class="widgets footer-widgets"><div class="container widget-wrapper">%1$s</div></aside>',
    $footerWidgets
  );
  return $widgets . $footer;
  return str_replace( '</footer>', '</footer>' . $widgets, $footer );
}
\add_filter( 'Boldface\Bootstrap\Views\footer', __NAMESPACE__ . '\footer', 20 );

/**
 * Disable autorow feature.
 *
 * @since 0.1
 */
\add_filter( 'Boldface\Bootstrap\Controllers\entry\autorow', '__return_false' );

/**
 * Disable REST feature.
 *
 * @since 0.1
 */
\add_filter( 'Boldface\Bootstrap\REST', '__return_false' );
