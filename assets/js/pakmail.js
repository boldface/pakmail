/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

var registerBlockType = wp.blocks.registerBlockType;
var _wp$editor = wp.editor,
    RichText = _wp$editor.RichText,
    InspectorControls = _wp$editor.InspectorControls,
    PanelColorSettings = _wp$editor.PanelColorSettings;
var Fragment = wp.element.Fragment;


registerBlockType('pakmail/cta', {
  title: 'Call to Action',
  icon: 'universal-access-alt',
  category: 'layout',
  attributes: {
    headline: {
      type: 'array',
      source: 'children',
      selector: 'h2'
    },
    content: {
      type: 'array',
      source: 'children',
      selector: 'p'
    },
    link: {
      type: 'array',
      source: 'children',
      selector: '.link'
    },
    backgroundColor: {
      type: 'string'
    }
  },
  edit: function edit(_ref) {
    var attributes = _ref.attributes,
        className = _ref.className,
        setAttributes = _ref.setAttributes;
    var headline = attributes.headline,
        content = attributes.content,
        link = attributes.link,
        textColor = attributes.textColor,
        backgroundColor = attributes.backgroundColor;

    var onChangeHeadline = function onChangeHeadline(newHeadline) {
      setAttributes({ headline: newHeadline });
    };
    var onChangeContent = function onChangeContent(newContent) {
      setAttributes({ content: newContent });
    };
    var onChangeLink = function onChangeLink(newLink) {
      setAttributes({ link: newLink });
    };
    var style = {
      backgroundColor: backgroundColor,
      color: textColor
    };
    return wp.element.createElement(
      'div',
      { className: className, style: style },
      wp.element.createElement(
        InspectorControls,
        null,
        wp.element.createElement(PanelColorSettings, {
          title: 'Color Settings',
          colorSettings: [{
            value: backgroundColor,
            onChange: function onChange(colorValue) {
              return setAttributes({ backgroundColor: colorValue });
            },
            label: "Background Color"
          }, {
            value: textColor,
            onChange: function onChange(colorValue) {
              return setAttributes({ textColor: colorValue });
            },
            label: "Text Color"
          }]
        })
      ),
      wp.element.createElement(RichText, {
        tagName: 'h2',
        value: headline,
        onChange: onChangeHeadline
      }),
      wp.element.createElement(RichText, {
        tagName: 'p',
        value: content,
        onChange: onChangeContent
      }),
      wp.element.createElement(RichText, {
        tagName: 'p',
        value: link,
        onChange: onChangeLink
      })
    );
  },
  save: function save(_ref2) {
    var attributes = _ref2.attributes,
        className = _ref2.className;
    var headline = attributes.headline,
        content = attributes.content,
        link = attributes.link,
        backgroundColor = attributes.backgroundColor,
        textColor = attributes.textColor;

    var style = {
      backgroundColor: backgroundColor,
      color: textColor
    };
    return wp.element.createElement(
      'div',
      { className: className, style: style },
      wp.element.createElement(
        'div',
        { className: 'container' },
        wp.element.createElement(
          'h2',
          null,
          headline
        ),
        wp.element.createElement(
          'p',
          null,
          content
        ),
        wp.element.createElement(
          'p',
          { 'class': 'link' },
          link
        )
      )
    );
  }
});

/***/ })
/******/ ]);