<?php

/**
 * @package Boldface\PakMail
 */
declare( strict_types = 1 );
namespace Boldface\PakMail;

/**
 * Class for updating the theme
 *
 * @since 0.1
 */
class updater extends \Boldface\Bootstrap\updater {
  protected $theme = 'pakmail';
  protected $repository = 'boldface/skulogic';
}
