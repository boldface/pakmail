(function($) {
  $( window ).scroll(function(){
    if( $(document).scrollTop() > 5 ) {
      $( '.navbar-brand img' ).addClass( 'scroll' );
    }
    else {
      $( '.navbar-brand img' ).removeClass( 'scroll' );
    }
  });
})(jQuery);
