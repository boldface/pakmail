var obj = {
  current: 0,
  question: "Got to mail a ##ITEM##",
  items: [
    "cello",
    "bike",
    "computer",
    "kayak",
  ]
};

(function($) {
  var i = 0;
  var p = $( '.wp-block-pakmail-cta h2' );
  $(p).text( obj.question.replace( '##ITEM##', '')).append('<span id="id">'+obj.items[i]+'?</span>');
  obj.current++;
  window.setInterval(event,3000);
  function event() {
    i = obj.current % obj.items.length;
    $('#id').fadeOut( 500, function() {
      $('#id').text( obj.items[i] + '?' ).fadeIn( 500 );
    });
    obj.current++;
  }
})(jQuery);
