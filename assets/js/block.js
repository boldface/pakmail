const { registerBlockType } = wp.blocks;
const { RichText, InspectorControls, PanelColorSettings } = wp.editor;
const { Fragment } = wp.element;

registerBlockType( 'pakmail/cta', {
  title: 'Call to Action',
  icon: 'universal-access-alt',
  category: 'layout',
  attributes: {
    headline: {
      type: 'array',
      source: 'children',
      selector: 'h2',
    },
    content: {
      type: 'array',
      source: 'children',
      selector: 'p',
    },
    link: {
      type: 'array',
      source: 'children',
      selector: '.link',
    },
    backgroundColor: {
      type: 'string',
    }
  },
  edit( { attributes, className, setAttributes } ) {
    const { headline, content, link, textColor, backgroundColor } = attributes;
    const onChangeHeadline = newHeadline => {
      setAttributes( { headline: newHeadline } );
    };
    const onChangeContent = newContent => {
      setAttributes( { content: newContent } );
    };
    const onChangeLink = newLink => {
      setAttributes( { link: newLink } );
    };
    const style = {
      backgroundColor: backgroundColor,
      color: textColor,
    };
    return (
      <div className={ className } style={ style }>
        <InspectorControls>
        <PanelColorSettings
          title="Color Settings"
          colorSettings={ [
            {
              value: backgroundColor,
              onChange: ( colorValue ) => setAttributes( { backgroundColor: colorValue } ),
              label: "Background Color",
            },
            {
              value: textColor,
              onChange: ( colorValue ) => setAttributes( { textColor: colorValue } ),
              label: "Text Color"
            },
          ] }
        ></PanelColorSettings>
        </InspectorControls>
          <RichText
            tagName="h2"
            value={ headline }
            onChange={ onChangeHeadline }
          />
          <RichText
            tagName="p"
            value={ content }
            onChange={ onChangeContent }
          />
          <RichText
            tagName="p"
            value={ link }
            onChange={ onChangeLink }
          />
      </div>
    )
  },
  save( { attributes, className } ) {
    const { headline, content, link, backgroundColor, textColor } = attributes;
    const style = {
      backgroundColor: backgroundColor,
      color: textColor,
    };
    return (
      <div className={ className } style={ style }>
        <div className="container">
          <h2>{ headline }</h2>
          <p>{ content }</p>
          <p class="link">{ link }</p>
        </div>
      </div>
    );
  }
} );
